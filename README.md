# README #

This is a single python script to send email through gmail smtp.  
It's possible to attach files.  
Compatible with Python 2 and 3.  

I think it's handy to have a possibility to clone repo and send email to myself from everywhere (while repairing my girlfriend's laptop or while connected to some device by ssh).

**CAUTION** you'll have to shine up your password to send an email (so I just use mock address, tssss)

## How to use

There is a help built in script:

```
usage: sendmail.py [-h] --login LOGIN --password PASSWORD --prefix PREFIX --to
                   TO [TO ...] [--subj SUBJ] [--body BODY]
                   [--files FILES [FILES ...]]

optional arguments:
  -h, --help            show this help message and exit
  --login LOGIN         Gmail email (login)
  --password PASSWORD   Gmail password
  --prefix PREFIX       Prefix in subject
  --to TO [TO ...]      Recievers (seperate with space)
  --subj SUBJ           Subject
  --body BODY           Body
  --files FILES [FILES ...]
                        Files (separate with space)
```