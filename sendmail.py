import argparse
import os
import sys

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import mimetypes
import os
import smtplib

SMTP_SERVER = "smtp.gmail.com"
SMTP_PORT = 587


def get_lines(filename):
    with open(filename) as f:
        lines = f.readlines()

    for i in range(0, len(lines)):
        lines[i] = lines[i].strip()

    return lines


def send_mail(login, password, prefix, receivers, subject, body, files=None):
    if files is None:
        files = []
    print('start sending email')
    msg = MIMEMultipart()
    msg["From"] = login
    msg["To"] = str(receivers)
    msg["Subject"] = prefix + ' ' + subject

    if len(files) > 0:
        for file_to_attach in files:
            if os.path.isfile(file_to_attach):
                ctype, encoding = mimetypes.guess_type(file_to_attach)
                if ctype is None or encoding is not None:
                    ctype = "application/octet-stream"

                maintype, subtype = ctype.split("/", 1)
                fp = open(file_to_attach, "rb")
                attachment = MIMEBase(maintype, subtype)
                attachment.set_payload(fp.read())
                fp.close()
                encoders.encode_base64(attachment)

                attachment.add_header("Content-Disposition", "attachment", filename=file_to_attach)
                msg.attach(attachment)
            else:
                body = body + "Can't found file" + file_to_attach

    msg.attach(MIMEText(body, 'html'))
    user = login
    pwd = password
    from_email = login

    try:
        server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        server.ehlo()
        server.starttls()
        server.login(user, pwd)
        server.sendmail(from_email, receivers, msg.as_string())
        server.close()
        print('Successfully sent the mail to \n %s \n' % (str(receivers)))

    except BaseException:
        print("Failed to send the mail")

if __name__ == '__main__':
    sys.dont_write_bytecode = True

    parser = argparse.ArgumentParser()
    parser.add_argument('--login', action='store', help='Gmail email (login)', required=True)
    parser.add_argument('--password', action='store', help='Gmail password', required=True)
    parser.add_argument('--prefix', action='store', help='Prefix in subject', required=True)
    parser.add_argument('--to', action='store', nargs='+', help='Recievers (seperate with space)', required=True)
    parser.add_argument('--subj', action='store', help='Subject', default='')
    parser.add_argument('--body', action='store', help='Body', default='')
    parser.add_argument('--files', action='store', nargs='+', help='Files (separate with space)', default=[])

    args = parser.parse_args()
    arg_receivers = list(args.to)
    arg_subject = '[' + str(args.prefix) + ']'
    arg_login = str(args.login)
    arg_pass = str(args.password)
    arg_prefix = str(args.prefix)
    arg_body = str(args.body)
    arg_files = list(args.files)

    send_mail(arg_login, arg_pass, arg_prefix, arg_receivers, subject=arg_subject, body=arg_body, files=arg_files)
